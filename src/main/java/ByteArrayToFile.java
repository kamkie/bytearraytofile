import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 31.10.2014.
 */
public class ByteArrayToFile {

    public static void main(String[] args) {
        ByteArrayToFile byteArrayToFile = new ByteArrayToFile();

        List<Integer> data = byteArrayToFile.readFile();

        byteArrayToFile.writeFile("data/message.zip", data);
    }

    public List<Integer> readFile() {
        List<Integer> bytes = new ArrayList<Integer>();
        InputStream resource = getClass().getResourceAsStream("data.txt");

        String data = "";

        try {
            data = IOUtils.toString(resource);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        String[] strInts = data.split(",");

        for (String strI : strInts) {
            bytes.add(Integer.parseInt(strI));
        }

        return bytes;
    }

    public void writeFile(String fileName, List<Integer> bytes) {
        File outFile = new File(fileName);

        if (!outFile.getParentFile().exists()) {
            outFile.getParentFile().mkdirs();
        }

        FileOutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(outFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            System.exit(1);
        }


        try {
            for (int b : bytes) {
                outputStream.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
